module FeatureFlagHelper
  def beta_test?(current_user:)
    if current_user.email == ENV['BETA_TESTER_EMAIL'] && ENV['EXPERIMENT_ACTIVE'] == 'true' && ENV['RAILS_ENV'] != 'test'
      return true
    end

    false
  end
end
