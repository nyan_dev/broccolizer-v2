class ApplicationController < ActionController::Base
  include FeatureFlagHelper

  before_action :authenticate_admin!
end
