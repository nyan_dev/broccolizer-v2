class DashboardsController < ApplicationController
  before_action :set_current_admin

  def show
  end

  private

  def set_current_admin
    @current_admin = current_admin
  end
end
