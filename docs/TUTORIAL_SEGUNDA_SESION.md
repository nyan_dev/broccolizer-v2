## Tutorial segunda sesión

En esta sesión vamos a centrarnos en planificar el producto y añadir funcionalidades utilizando buenas prácticas de desarrollo.


## Requisitos del sistema

- Tener instalada y funcionando en local la aplicación contruida en la [primera sesión](TUTORIAL_PRIMERA_SESION).
- Haber completado las historias que se encuentran en "Done" en este [tablero](https://trello.com/b/VTHwrp39/broccolizer-kanban) (Encuentra los commits en los comentarios de su respectiva tarjeta)

## Índice

1. [Product owning](#product-owning)
    1. [MVP](#mvp)
    2. [User story](#user-story)
    3. [Technical task](#technical-task)
2. [Desarrollo](#desarrollo)
    1. [Trunk based development](#trunk-based-development)
    2. [Feature Flags](#feature-flags)
    3. [TDD](#tdd)
    4. [Baby steps](#baby-steps)


## Product owning

Como comentamos en la primera sesión, queremos que nuestro ciclo de desarrollo siga un modelo iterativo: Una aplicación que es útil desde el principio y va escalando de forma iterativa hacia lo que mejor se adapte a las necesidades de cliente. Pero ¿cuáles son las necesidades de cliente? Esto es lo que tenemos que investigar en la parte de desarrollo de producto.

Para mí la parte de producto es tan necesaria en un perfil fullstack como la parte de infraestructura o de programación. De hecho no creo que sea posible crear un software útil si carecemos de empatía. La programación es ante todo un trabajo empático y creativo, por lo tanto es importante saber diseñar el producto antes de llegar a un editor de código.

Para intentar transmitiros esto voy a mostraros el flujo que en mi experiencia trabajando en empresas con distintas idiosincrasias es el que mejor funciona y que es también el que más a menudo he visto en equipos de alto rendimiento. Este flujo consiste en los siguientes pasos, que he intentado reproductir en este [tablero de Trello](https://trello.com/b/VTHwrp39/broccolizer-kanban):

### MVP

El MVP, Minimum Valuable Product recoge qué debe tener nuestra aplicación como mínimo para que le sirva a nuestro público objetivo (si fuera el caso, lo mínimo para empezar a ser monetizada). [Aquí](https://trello.com/c/MWcbGmlI/1-mvp) puedes ver el MVP de Broccolizer.

### User story

Las User Stories, Historias de Usuario en español, en adelante US, son las funcionalidades que podemos sacar en cada iteración. En un squad ágil estas US son construidas por la persona encargada del producto (PO, PM, en adelante "Producto") en colaboración con stakeholders (clientes, usuarios...) y la gente de desarrollo (en adelante "Desarrollo"), de una forma estructurada. En nuestro [tablero](https://trello.com/b/VTHwrp39/broccolizer-kanban) puedes ver 4 columnas:

- **Backlog**: Aquí es donde nacen las US. Producto extrae una necesidad del MVP, por ejemplo, la necesidad de que los perfiles tengan alimentos asociados. Crea una US en la columna "Backlog" titulada "Añadir un nuevo alimento". A través de conversaciones, encuestas, estadísticas etc, va obteniendo información sobre cómo satisfacer esa necesidad (ofrecer una pantalla donde introducir el alimento, evitar que los alimentos se repitan, guardar los alimentos introducidos...). Conforme va aterrizando las ideas muestra la historia a Desarrollo, con quienes mantendrá una serie de debates para ajustar la solución a las posibilidades del sistema.
- **Accepted**: Cuando la historia está totalmente aterrizada y toda la información necesaria para desarrollarla está redactada en ella, Desarrollo la "acepta". Esto significa que la historia está "completa" y será desarrollada cuando sea una prioridad. La prioridad de las US se determina entre Producto y Desarrollo.
Una US "completa" idealmente contiene:
  - _Valor_: Qué valor me aporta esta funcionalidad como usuaria.
  - _Contexto_: En qué estado se encuentra ahora mismo la aplicación con respecto a esta funcionalidad.
  - _Descripción_: Descripción detallada de lo que necesitamos añadir a la aplicación, SIN entrar en cuestiones técnicas. Aquí puede añadirse ejemplos de diseño, links a recursos necesarios, ejemplos de casos de uso...
  - _DoD_: `Definition of Done` Qué debe contener la apliación para determinar que la tarea está hecha.

Esto es tan sólo una plantilla que podemos utilizar si nos ayuda a recordar lo que necesita Desarrollo, pero realmente una historia estará "completa", es decir, lista para ser desarrollada, cuando pueda ser ejecutada por cualquier persona del equipo de desarrollo. Por eso es necesario especificarlo todo y no dar nada por hecho.

- **In progress**: Esta columna contiene las US en las que se está trabajando ya. Las US que se encuentren en esta columna deben estar divididas en [tareas técnicas](#technical-task).
- **Done**: Esta columna contiene las US que están terminadas. Una historia está "Terminada" cuando está funcionando en producción.

Así es como quedaría nuestra US ["Añadir un nuevo alimento"](https://trello.com/c/fyNUg8Di/6-a%C3%B1adir-nuevo-alimento).

Idealmente las US fluirán de izquierda a derecha (Backlog -> Accepted -> In progress -> Done) y si nos dejáramos algo por el camino escribiríamos una nueva US en vez de dar marcha atrás.
Algunas empresas utilizan rituales semanales como la `synchro` para sincronizar a Producto con Stakeholders y Desarrollo, aunque también puede funcionar bien de forma asíncrona o con reuniones puntuales.

### Technical task

Las tareas técnicas son creadas por Desarrollo a partir de una US, y en ella expresamos qué debemos cambiar en el código para hacer realidad esa funcionalidad concreta. Idealmente, estas tareas deben ser independientes (no depender del orden en el que se hagan) y deben estar bien definidas para que la persona o personas que vayan a trabajar sobre ella puedan terminarla de forma autónoma.
Puedes ver ejemplos muy resumidos de tareas técnicas en la última Checklist de cada US en el [tablero](https://trello.com/b/VTHwrp39/broccolizer-kanban).

## Desarrollo

Una vez tenemos nuestras tareas técnicas, ya podemos empezar el desarrollo.

### Trunk based development

Para el desarrollo vamos a utilizar la técnica del Trunk Based Development: Desarrollo en la rama principal.

Es común en las empresas utilizar ramas de desarrollo o de testing, que viven durante un tiempo (el tiempo que tarden en aprobarte la PR o en probar tu funcionalidad) lo cual tiene sus pros y sus contras. El Trunk based defiende que todos los equipos trabajen sobre la misma rama, la rama principal, y se pruebe en producción.

- Ventajas:
  - No tienes ningún bloqueo para desplegar a producción: PRs, code reviews, aprobación en staging…
  - Pruebas tus funcionalidades en producción. No en un entorno parecido, sino en producción. Por lo tanto las pruebas son muy fiables.

- Inconvenientes:
  - Obvimente esto no puede hacerse a lo loco, sino que debemos tener una fortísima disciplina y una fortísima cultura de desarrollo ágil, y esto requiere formación.

Hoy vamos a utilizar tres técnicas básicas de este tipo de desarrollo:
  - Feature flags: Son unas marcas que vamos a poner en el código para activarlo y desactivarlo.
  - TDD: Vamos a utilizar los tests para diseñar el código en vez de escribirlo a posteriori, por lo que virtualmente todo el código que escribamos estará soportado por un test automático y podremos manipular el código con confianza.
  - Baby Steps: Los commits van a ir directos a producción, por lo tanto deben ser lo más atómicos posibles.

### Feature Flags

Las feature flags son "banderas" que pueden levantarse o bajarse para activar el código en situaciones concretas. Cuando desplegamos una funcionalidad con la intención de probarla en producción, no queremos que esté disponible para todo el mundo, sino que la querremos disponible solamente para unos perfiles y bajo unas condiciones concretas.

A continuación vamos a empezar el desarrollo con la primera tarea técnica relacionada con la US ["Añadir un nuevo alimento"](https://trello.com/c/fyNUg8Di/6-a%C3%B1adir-nuevo-alimento). La tarea consiste en añadir al menú la opción "Añadir alimento".

En primer lugar vamos a añadir la opción al menú, pero sólo si el perfil es de beta tester:

```ruby
# app/views/dashboards/show.html.erb

<%=
  if beta_test?(current_user: @current_admin)
    link_to 'Añadir alimento', '#', :method => :get , :class => 'nav-link'
  else
    link_to 'Próximamente', '#', :method => :get , :class => 'nav-link'
  end
%>
```

Para que esto funcione necesitaremos conocer `@current_admin`:

```ruby
# app/controllers/dashboards_controller.rb

class DashboardsController < ApplicationController
  before_action :set_current_admin

  # [...]

  private

  def set_current_admin
    @current_admin = current_admin
  end
end
```

También necesitamos determinar el valor del método `beta_test?`, esta es nuestra feature flag. Queremos activar la nueva feature sólo si:
- El perfil actual es beta tester (Variable de entorno `BETA_TESTER_EMAIL`)
- El experimento está activo (Variable de entorno `EXPERIMENT_ACTIVE`)
- No estamos desarrollando o lanzando tests automáticos (Variable de entorno `RAILS_ENV`)

```ruby
# app/helpers/feature_flag_helper.rb

module FeatureFlagHelper
  def beta_test?(current_user:)
    if current_user.email == ENV['BETA_TESTER_EMAIL'] && ENV['EXPERIMENT_ACTIVE'] == 'true' && ENV['RAILS_ENV'] != 'test'
      return true
    end

    false
  end
end

# app/controllers/application_controller.rb

class ApplicationController < ActionController::Base
  include FeatureFlagHelper

  # [...]
end
```

Comprobamos si está funcionando en local. En caso afirmativo hacemos el [commit](https://gitlab.com/nyan_dev/broccolizer-v2/-/commit/ac46e45d4c78afcd00bdb99e8be36147516026b8) y pusheamos. Como tenemos configurado el despliegue automático de la [primera sesión](TUTORIAL_PRIMERA_SESION), nuestro commit irá directo a master. Cuando el despliegue se complete podremos comprobar si la funcionalidad aparece bajo las condiciones especificadas.

Si todo funciona bien, podremos quitar las feature flags cuando la US esté terminada.

### TDD

TDD significa "Diseño basado en pruebas". Las pruebas o tests son los que provocan que se escriba el código, no al revés. Esto se explica mejor en directo, aunque durante la sesión nos quedamos justas de tiempo para explicar esta parte, de forma que no encontrarás ejemplos de eso en el repo.

Por lo tanto voy a poner un ejemplo muy básico de cómo podemos utilizar los test para diseñar el código. Si quieres algo más currado tal vez quieras leer [este artículo](https://medium.com/@FlywireEng/katayuno-c2c19c65e66a) que escribí hace tiempo sobre el tema, en el que también se describen otras prácticas, como el pairing y el pomodoro.

Si prefieres seguir con la temática de Broccolizer, vamos a trabajar con la segunda tarea técnica de [esta US](https://trello.com/c/fyNUg8Di/6-a%C3%B1adir-nuevo-alimento), que consiste en crear un modelo `Food` que contiene el campo obligatorio `name` asociado a una categoría y a un perfil y guarda los valores formateados (capitalizado y sin símbolos) para evitar duplicados.

Para las restricciones de campo podemos utilizar [shoulda matchers](https://github.com/thoughtbot/shoulda-matchers):

```ruby
# spec/models/food_spec.rb

require 'rails_helper'

RSpec.describe Food, type: :model do
  describe 'validations' do
    it { should belong_to(:category) }
    it { should belong_to(:admin) }
    it { should validate_presence_of(:name) }
  end
end
```

La primera vez que lancemos el test nos dirá que el modelo `Food` no existe, lo que nos obligará a crearlo:

```ruby
# app/models/food.rb

class Food < ApplicationRecord
end
```

La segunda vez que lo lancemos, nos dirá que no estamos cumpliendo ninguna de las condiciones (`belong_to(:category)`, `belong_to(:admin)`, `validate_presence_of(:name)`), por lo que deberemos añadir estas restricciones al modelo:

```ruby
# app/models/food.rb

class Food < ApplicationRecord
  belongs_to :category
  belongs_to :admin
  validates :name, presence: true
end
```

Esta vez todos los tests estarán "en verde" (los tests pasan). Si no deseamos "refactorizar" nada, ya podríamos commitear este pequeño avance y pushearlo a master.

A continuación vamos a crear un test para comprobar si estamos capitalizando el valor de `name` antes de guardarlo:

```ruby
# spec/models/food_spec.rb

# [...]

it 'capitalizes names before saving' do
  create(:food, name: 'cAPITalIZE')

  expect(Food.where(name: 'Capitalize').count).to eq(1)
end
```

Obviamente este test estará "en rojo" hasta que añadamos la funcionalidad:

```ruby
# app/models/food.rb

class Food < ApplicationRecord
  # [...]

  def name=(value)
    write_attribute(:name, normalize(value))
  end

  private

  def normalize(value)
    value.capitalize unless value.nil?
  end
end
```

Finalmente repetiremos el proceso para eliminar los símbolos antes de guardar el valor.

Fíjate que a lo largo del proceso he estado hablando de tests "en rojo", "en verde" y de "refactorizar", esto se debe a que el TDD se desarrolla en ciclos de 3 fases:
- **Red**: El test está escrito pero la funcionalidad no, por lo tanto las condiciones del test no se cumplen.
- **Green**: La funcionalidad está escrita y por lo tanto las condiciones del teste se cumplen.
- **Refactor**: El código de la funcionalidad se ha estructurado de forma que sea legible y haya quedado limpio ("Clean Code").

En esta parte no he utilizado Feature Flags con el objetivo de centrar la explicación en el TDD pero obviamente estas técnicas deben ser combinadas.

### Baby steps

Como puedes ver, la técnica "Divide y vencerás" es muy útil ya que nos permite desarrollar nuestra aplicación de una forma incremental y segura, además de dar mucha autonomía a Desarrollo. Cuando la US esté terminada podremos liberarla para que pueda ser utilizada en su totalidad por todos los perfiles pero, en última instancia, la aplicación habrá crecido test a test.
