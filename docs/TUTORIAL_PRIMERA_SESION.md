## Tutorial primera sesión

[Slides](https://docs.google.com/presentation/d/1QguIhIZOYmUzT0_isQ0otKQsOFwiz2nPrYKUM9wiE3g/edit?usp=sharing)

En este tutorial se explican los pasos a seguir para crear una aplicación de Rails y configurar un sistema de despliegue automático.

## Requisitos del sistema

- **Ubuntu 20.04 LTS (Focal Fossa):** Es el SO en el que se realizó el taller, aunque es posible que funcione con otras distros o en otros sistemas operativos.
- **[Docker](https://www.docker.com/)**
- **[Docker Compose](https://docs.docker.com/compose/)**
- **[Yarn](https://yarnpkg.com/):** Es importante que tengas la última versión de Yarn.

## Índice

1. [Herramientas](#herramientas)
    1. [Ruby on Rails](#ruby-on-rails)
    2. [PostgreSQL](#postgresql)
    3. [Docker](#docker)
    4. [Gitlab](#gitlab)
    5. [Heroku](#heroku)
2. [Pasos](#pasos)
    1. [Crear el repositorio](#crear-el-repositorio)
    2. [Levantar la aplicación en local](#levantar-la-aplicación-en-local)
        1. [Definir las dependencias](#definir-las-dependencias)
        2. [Definir la infraestructura](#definir-la-infraestructura)
        3. [Iniciar la applicación](#iniciar-la-applicación)
    3. [Configurar el despliegue automático](#configurar-el-despliegue-automático)
        1. [Crear el servidor](#crear-el-servidor)
        2. [Convertir nuestra aplicación en desplegable](#convertir-nuestra-aplicación-en-desplegable)
        3. [Configurar los pipelines](#configurar-los-pipelines)

## Herramientas

### Ruby on Rails

Como lenguaje de programación vamos a usar [Ruby](https://www.ruby-lang.org/en/), concretamente el framework [Ruby on Rails](https://rubyonrails.org/). He elegido este lenguaje porque estoy muy familiarizada con él y además es muy cómodo para crear aplicaciones web completas, es decir que gestionen tanto el front como el back.

### PostgreSQL

Como base de datos utilizaré [PostgreSQL](https://www.postgresql.org/) por su compatibilidad con el resto de tecnologías que usaremos

### Docker

Para trabajar en local voy a contener mi aplicación con [Docker](https://www.docker.com/). Un contenedor de Docker es básicamente una máquina virtual que se configura super fácil con un par de ficheros. Prefiero dockerizar mis aplicaciones porque de esta forma cualquiera que tenga esos dos ficheros puede ejecutar la aplicación en local aunque no tenga nada más instalado, por lo que es muy cómodo para compartir el entorno de desarrollo.

### GitLab

Como repositorio de versiones voy a utilizar [Gitlab](https://gitlab.com/). Esta herramienta me va a permitir gestionar tanto las versiones de código como los pipelines, es decir, toda la cadena de montaje, desde que me llegan las piezas hasta que entrego el producto.
Suelo utilizar esta herramienta principalmente porque es open source, tiene muchas funcionalidades aparte del control de versiones y ofrece repositorios privados gratuitos.

### Heroku

Finalmente utilizaré un servidor de [Heroku](heroku.com) para hostear mi web. Heroku es una herramienta que proporciona como servicio una plataforma, es decir, que ofrece [Platform as a Service](https://en.wikipedia.org/wiki/Platform_as_a_service). Cuando hablamos de "platform" nos referimos al lugar donde se aloja nuestra aplicación, en este caso, el servidor de Heroku.
Lo he elegido porque es muy fácil de configurar y es gratuito, por lo tanto es perfecto para proyectos personales y profesionales.

## Pasos

### Crear el repositorio

- Vamos a [Gitlab](https://gitlab.com/) (Tendrás que hacerte una cuenta si no la tienes)
- Hacemos click en `New project`
- Como `Project name` le ponemos el nombre de la aplicación, en este caso broccolizer-v2
- Como `Project description` le ponemos `This application helps you keep track of your diet`
- Comprobamos que `Visibility Level` sea `Public`
- Hacemos click en `Create project`
- En nuestro ordenador abrimos una consola y nos clonamos el proyecto:

```bash
$ git clone git@gitlab.com:nyan_dev/broccolizer-v2.git
```

### Levantar la aplicación en local

#### Definir las dependencias

En primer lugar tenemos que definir las dependencias y los comandos necesarios para levantar nuestra aplicación. Esto se realiza en el fichero Dockerfile.

En este caso nuestra aplicación necesita:
- **Ruby**, que es la base de Rails
- **NodeJS**, que es el runtime de JavaScript que utiliza Rails
- **Yarn**, que es el gestor de paquetes que utiliza Rails
- **PostgreSQL**, que será nuestra base de datos

Y aparte de esto tendremos que indicarle los pasos a seguir para levantar la aplicación.
Vamos a verlo mejor a través del código:

- Con nuestro editor preferido (yo utilizo [Visual Studio Code](https://code.visualstudio.com/)) abrimos la aplicación que nos hemos clonado en el paso anterior. A partir de ahora todos los pasos se realizarán en la carpeta del proyecto (`broccolizer-v2`)
- Nos creamos un fichero nuevo con el nombre `Dockerfile`.
- Escribimos las instrucciones en forma de comandos Unix (es el SO que lleva el contenedor de Docker):

```bash
FROM ruby:2.7

# Descargar los repositorios necesarios
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo 'deb https://dl.yarnpkg.com/debian/ stable main' | tee /etc/apt/sources.list.d/yarn.list

# Instalar las dependencias
RUN apt-get update && apt-get install -y nodejs yarn postgresql-client

# Configurar el entorno de trabajo
RUN mkdir /broccolizer-v2
WORKDIR /broccolizer-v2
COPY Gemfile /broccolizer-v2/Gemfile
COPY Gemfile.lock /broccolizer-v2/Gemfile.lock
RUN bundle install
COPY . /broccolizer-v2

# Configurar el entrypoint de Rails
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

# Levantar el proceso principal
CMD ["rails", "server", "-b", "0.0.0.0"]
```

Ahora debemos crear todos los ficheros a los que hacemos referencia en el Dockerfile:

- Fichero `Gemfile`, indicando la instalación de Ruby on Rails

```bash
# frozen_string_literal: true

source "https://rubygems.org"

git_source(:github) {|repo_name| "https://github.com/#{repo_name}" }

# Indicamos que debe instalarse la gema "rails"
gem "rails"
```

- Fichero `Gemfile.lock` (vacío)
- Fichero `entrypoint.sh`

```bash
#!/bin/bash
set -e

# Remove a potentially pre-existing server.pid for Rails.
rm -f /broccolizer-v2/tmp/pids/server.pid

# Then exec the container's main process (what's set as CMD in the Dockerfile).
exec "$@"
```

- Commiteamos nuestros cambios:

```bash
$ git add .
$ git commit -m "Setup project dependencies"
```
([commit](https://gitlab.com/nyan_dev/broccolizer-v2/-/commit/955ad4ea72735d3aed0a8f8a38bc6d5129a6b815))

- Antes de subir el commit, nos preocupamos de renombrar la rama principal, ya que por defecto git nos la nombra `master` (sólo es necesario hacerlo una vez):

```bash
$ git branch -m main
```

- Y ya podemos subir nuestros cambios al repositorio de GitLab:

```bash
$ git push origin main
```

Si vamos a GitLab debemos ver nuestro primer commit :)

#### Definir la infraestructura

Ya tenemos la parte de las dependencias, ahora vamos a definir la infraestructura.
Nuestra infraestructura consiste en:
- Un servidor web donde está corriendo la aplicación de rails (servicio `web`)
- Una base de datos (BDD) de la que tira nuestra aplicación (servicio `db`)

Cada uno de estos servicios estará alojado en su propio contenedor pero en el caso del servidor web le diremos que espere a que la base de datos esté disponible.

- En la carpeta del proyecto, creamos el fichero `docker-compose.yml`, en el que indicamos la infraestructura de servicios:

```bash
version: '3'

services:
  web: # Servidor web
    build: .
    command: bash -c "rm -f tmp/pids/server.pid && bundle exec rails s -p 3000 -b '0.0.0.0'" # Borramos pids antiguos y levantamos el servidor de Rails en el puerto 3000
    volumes:
      - .:/broccolizer-v2
    ports:
      - '3000:3000' # Mapear el puerto 3000 de nuestro ordenador con el puerto 3000 del contenedor
    depends_on:
      - db # Indicamos la dependencia de la BDD (servicio db)

  db: # BDD
    image: postgres # Nos descargamos una máquina virtual que ya viene con PostgreSQL configurada
    volumes:
      - ./tmp/db:/var/lib/postgresql/data
    environment:
      POSTGRES_PASSWORD: password # Indicamos la password que queremos darle a la BDD
```

- Commiteamos nuestros cambios

```bash
$ git add .
$ git commit -m "Setup infrastructure"
$ git push origin main
```
([commit1](https://gitlab.com/nyan_dev/broccolizer-v2/-/commit/85ed4ba35b4f795dbce555dfbbeda2068e46895c))
([commit2](https://gitlab.com/nyan_dev/broccolizer-v2/-/commit/20cafe3df1fd470898e218d4a0cd532a6fa7f094))

#### Iniciar la applicación

Ya hemos definido lo que necesitamos, pero ahora tenemos que ejecutar esos comandos que hemos indicado.

- Iniciamos una nueva aplicación de Rails en el contenedor donde se encuentra el servicio `web`, indicando que vamos a utilizar postgresql como base de datos:

```bash
$ docker-compose run web rails new . --force --no-deps --database=postgresql
```

Si no indicamos la base de datos, el comando `rails new` configurará una base de datos por defecto, posiblemente `SQLite`, y habremos de reconfigurarlo.

En Ubuntu es posible que el comando falle por permisos la primera vez que lo ejecutamos, para resolverlo añadimos nuestra usuaria local como propietaria del proyecto y volvemos a intentarlo:

```bash
$ sudo chown -R $USER:$USER .
$ docker-compose run web rails new . --force --no-deps --database=postgresql
```

Cuando el proceso termine, veremos que en nuestra carpeta se ha creado un nuevo proyecto de Rails, con toda la estructura de ficheros.

Por último, reconstruimos la imagen para que se configuren las dependencias del Gemfile:

```bash
$ docker-compose build
```

Aprovechamos el tiempo que tarda este comando para incluir en nuestro .gitignore el fichero de secretos que usaremos en los siguientes pasos. Buscamos el fichero `.gitignore` en el proyecto, y le añadimos la línea

```bash
/config/.env
```
[ver fichero `.gitignore` completo](../.gitignore)

También es buen momento para arreglar el Readme ([ver](../README.md))

Una vez termine la reconstrucción de la imagen, podemos intentar levantar el proyecto:

```bash
$ docker-compose up
```

Cuando aparezca el mensaje `* Listening on tcp://0.0.0.0:3000`, abrimos la siguiente URL en nuestro navegador preferido:

```bash
localhost:3000
```

Veremos un error de Base de Datos. Aunque Rails es bastante dramático mostrando errores, el que nos muestra indica que vamos por buen camino. Nuestro servidor web está bien configurado y está intentando acceder a la BDD que hemos indicado, pero falla porque aún no la hemos configurado.

Commiteamos este paso:

```bash
$ git add .
$ git commit -m "Initialize rails app"
$ git push origin main
```
([commit](https://gitlab.com/nyan_dev/broccolizer-v2/-/commit/6fe822cfa9e4f4a5bcba63119fb7ad69b19bbb8d))

Y nos ponemos a configurar la BDD:

- Abrimos el fichero `config/database.yml` y lo editamos de la siguiente forma:

```bash
default: &default
  adapter: postgresql
  encoding: unicode
  host: db # El host es el servicio db (situado en el otro contenedor)
  username: <%= ENV["DB_USER"] %> # Buscar el usuario en la variable de entorno DB_USER
  password: <%= ENV["DB_PASSWORD"] %> # Buscar el usuario en la variable de entorno DB_PASSWORD
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>

development: # BDD a usar mientras desarrollamos
  <<: *default
  database: broccolizer_v2_development

test: # BDD a usar mientras lanzamos la suite de test
  <<: *default
  database: broccolizer_v2_test

production: # BDD a usar en produccion
  <<: *default
  database: broccolizer_v2_production
```
Como ves hemos indicado que busque la información sensible en una variable de entorno. Estas variables de entorno las vamos a almacenar en local en el fichero de secretos que habíamos indicado en el .gitignore.

- En la carpeta `config` creamos el fichero `.env` con el siguiente contenido:

```bash
DB_PASSWORD=password
DB_USER=postgres
```

- Ahora editamos el fichero `docker-compose.yml` para indicar dónde está este fichero:

```bash
version: '3'

services:
  web:
    build: .
    command: bash -c "rm -f tmp/pids/server.pid && bundle exec rails s -p 3000 -b '0.0.0.0'"
    volumes:
      - .:/broccolizer-v2
    ports:
      - '3000:3000'
    depends_on:
      - db
    env_file: # Añadimos la ruta al fichero de secretos:
      - config/.env

  db:
    image: postgres
    volumes:
      - ./tmp/db:/var/lib/postgresql/data
    # Borramos las siguientes lineas:
    # environment:
      # POSTGRES_PASSWORD: password
```

Finalmente, generamos la base de datos:

```bash
docker-compose run web rails:create
```

Una vez hemos hecho esto ya podemos volver a levantar nuestra aplicación

```bash
docker-compose up
```

Y si accedemos a la URL `localhost:3000` deberíamos ver el mensaje de bienvenida de Rails.

No olvides commitear este cambio:

```bash
$ git add .
$ git commit -m "Configure database"
$ git push origin main
```
([commit](https://gitlab.com/nyan_dev/broccolizer-v2/-/commit/b843f02352065f7b608b6411933e213dc6c24155))

### Configurar el despliegue automático

Y ya sólo nos queda configurar el pipeline de despliegue automático, es decir que cada vez que se suba un cambio a nuestro repositorio de gitlab, se despliegue automáticamente en nuestro servidor público, es decir que quienes utilicen nuestra aplicación puedan ver los cambios en la web.

Para ello necesitaremos:

- Un servidor web en Heroku donde servir nuestra página
- Una aplicación de Ruby on Rails que pueda instalarse en ese servidor de Heroku
- Una serie de pipelines en GitLab que definan las comprobaciones que debemos hacer y dónde se encuentra el servidor

#### Crear el servidor

- Abrimos [heroku](heroku.com) (tendrás que crearte una cuenta en caso de no tenerla)
- Hacemos click en `New` -> `Create new app`
- En `App name` introducimos `broccolizer-v2`
- En `Choose a region` introducimos `Europe`
- Hacemos click en `Create app`

Ya tenemos creado nuestro servidor en Heroku.

#### Convertir nuestra aplicación en desplegable

Para que una aplicación de RoR sea desplegable en Heroku, necesitamos:
- Crear una página de inicio propia
- Indicar que los assets deben compilarse en el entorno de producción
- Crear un fichero Procfile

Las características de RoR las veremos en la próxima sesión, de forma que voy a centrarme en explicar los pasos. Te dejo unos links por si quieres ir mirando cómo funciona RoR.

Para crear la página de inicio, necesitamos un nuevo [controlador](https://www.tutorialspoint.com/ruby-on-rails/rails-controllers.htm):

```bash
$ docker-compose run web rails generate controller Welcome
```

Ahora creamos una [view](https://www.tutorialspoint.com/ruby-on-rails/rails-views.htm) correspondiente a este controlador:
En la carpeta `app/views/welcome/` creamos el fichero `index.html.erb` con el siguiente contenido:

```html
<h1>Welcome to Broccolizer</h1>
<p>This application helps you keep a track of your diet.</p>
```

Finalmente debemos indicar la [ruta](https://www.tutorialspoint.com/ruby-on-rails/rails-routes.htm) a esta página:
Editamos el fichero `config/routes` de la siguiente forma:
```ruby
Rails.application.routes.draw do
 get 'welcome/index'

 root 'welcome#index'
end
```

Si volvemos al navegador, al refrescar la página `localhost:3000` en vez del mensajito de Rails, veremos nuestra nueva página de inicio.

Commiteamos los cambios:

```bash
$ git add .
$ git commit -m "Create welcome page"
$ git push origin main
```
([commit](https://gitlab.com/nyan_dev/broccolizer-v2/-/commit/9bc6d94313feeabaed83388d49cb5b2a9f6b5225))

Después configuramos los assets para que se compilen en el entorno de producción; en el fichero `config/environments/production` editamos la línea:

```ruby
config.assets.compile = true
```
([commit](https://gitlab.com/nyan_dev/broccolizer-v2/-/commit/71a278f681c49ecdd8f71df47a169a9ad0287acc))

Finalmente creamos el fichero `Procfile`: ([commit](https://gitlab.com/nyan_dev/broccolizer-v2/-/commit/552bea80d6eedbb1d4b828046f2eb41db613dcb3))

Nuestra aplicación ya es desplegable en Heroku.


#### Configurar los pipelines

Por último, debemos configurar el pipeline de despliegue. Para ello necesitaremos:
- Crear el fichero `.gitlab-ci.yml`
- Crear las variables necesarias en GitLab

Vamos al editor y creamos el fichero `.gitlab-ci.yml`:

```bash
image: ruby:2.7

cache:
  paths:
    - vendor/bundle
    - node_modules

services:
  - postgres:10.1

variables: # Variables que necesitará en el proceso de construcción y despliegue
  BUNDLE_PATH: vendor/bundle
  DISABLE_SPRING: 1
  DB_HOST: $DB_HOST
  DB_USER: $DB_USER
  DB_PASWORD: $DB_PASWORD

stages:
  - build
  - deploy

build: # Paso de construcción
  stage: build
  script:
    - wget -q -O - https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
    - echo echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
    - apt-get update -qq && apt-get install -y nodejs yarn postgresql-client
    - bundle install --jobs $(nproc)
    - cp config/database.yml.ci config/database.yml
    - bundle exec rails db:create RAILS_ENV=test
    - bundle exec rails db:schema:load RAILS_ENV=test

deploy: # Paso de despliegue: Sólo se ejecutará si el paso de construcción termina correctamente
  stage: deploy
  only:
    - main # Desplegar sólo la rama estable (main)
  script:
    - curl https://cli-assets.heroku.com/install.sh | sh
    - gem install dpl  # Utiliza la gema dpl para desplegar la aplicación en Heroku
    - dpl --provider=heroku --app=$HEROKU_APP_PRODUCTION --api-key=$HEROKU_API_KEY
    - heroku run rake db:migrate --exit-code --app=$HEROKU_APP_PRODUCTION
```

Antes de subir este cambio al repositorio, indicamos el valor de las variables que estamos utilizando en el fichero. Para ello, en el repositorio de GitLab:
- En el menú de la izquierda seleccionamos `Settings` -> `CI/CD`
- Buscamos `Variables` y pulsamos en `Expand`
- Hacemos click en `Add Variable`
- En `Key` introducimos el nombre de la variable, y en `Value` el valor de la variable
- Hacemos click en `Add variable`
- Repetimos para cada variable:

| Key                   | Value                | Notas                                                                                             |
|-----------------------|----------------------|---------------------------------------------------------------------------------------------------|
| DB_PASSWORD           | postgres             | Al desplegarse en producción Heroku pondrá una contraseña segura                                  |
| DB_USER               | postgres             | Al desplegarse en producción Heroku pondrá otro usuario                                           |
| DB_HOST               | postgres             | Al desplegarse en producción Heroku pondrá otro host                                        |
| HEROKU_API_KEY        | Tu API_KEY de Heroku | La encontrarás haciendo click sobre tu perfil en Heroku > Account and Settings > API Key > Reveal |
| HEROKU_APP_PRODUCTION | broccolizer-v2       |                                                                                                   |

Ya podemos commitear nuestros cambios y ver cómo se despliega nuestra aplicación:

```bash
$ git add .
$ git commit -m "Setup pipelines"
$ git push origin main
```
([commit1](https://gitlab.com/nyan_dev/broccolizer-v2/-/commit/96e18b83c59f35483256ff54883ad7d4f67a2b2b))
([commit2](https://gitlab.com/nyan_dev/broccolizer-v2/-/commit/fa2cc70531298fbbde825dc86d1f5fdac265842d))

Vamos al repositorio en GitLab y en el menú izquierdo seleccionamos `CI /CD` -> `Pipelines`

Veremos que ya se está ejecutando la secuencia que hemos indicado en el fichero `.gitlab-ci.yml`:
- En la columna `stages` vemos dos circulitos, si nos ponemos encima con el ratón veremos que cada uno corresponde a un stage (`build` y `deploy`).
- Si hacemos click en uno de ellos y luego click en el modal que aparece debajo, podremos ver lo que está haciendo (los logs).

Si todo va bien, cuando ambos procesos terminen, deberíamos poder ver nuestra página de inicio en la página web ofrecida por Heroku, para ello abrimos el dashboard del proyecto en Heroku y hacemos click en `Open app`.

Felicidades, ya tenemos un sistema de despliegue continuo configurado. Ahora, cada vez que subamos un commit a GitLab, en la ventana de `Pipelines` aparecerá una nueva entrada y podremos ver si se construye y se despliega bien.
