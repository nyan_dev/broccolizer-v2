Category.create([
  { name: 'Carne blanca' },
  { name: 'Carne roja' },
  { name: 'Cereales' },
  { name: 'Fruta' },
  { name: 'Huevos' },
  { name: 'Lácteos' },
  { name: 'Legumbre' },
  { name: 'Marisco' },
  { name: 'Pescado azul' },
  { name: 'Pescado blanco' },
  { name: 'Verdura' }
])
